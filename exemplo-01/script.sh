kafka-topics --create --topic users --partitions 2 --bootstrap-server kafka:9092

kafka-console-consumer --topic users --from-beginning --bootstrap-server localhost:9092