from kafka import KafkaConsumer

topic_name = 'users'

consumer = KafkaConsumer(topic_name, group_id='my_group')
for msg in consumer:
    print(msg)