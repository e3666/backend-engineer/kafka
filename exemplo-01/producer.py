from sys import argv
from kafka import KafkaProducer

topic_name = 'users'
msg = argv[1]

# A-M = 0, N-Z = 1
partition = 0 if msg[0].upper() < 'N' else 1

producer = KafkaProducer(
    bootstrap_servers='localhost:9092'
)

# o valor precisa ser no formato binário
producer.send(
    topic=topic_name, 
    value=msg.encode(), 
    partition=partition
)

producer.flush()