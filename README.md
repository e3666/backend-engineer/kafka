# Sumário

- [**O que é**](#o-que-é)
- [**Queue vs Pub/Sub**](#queue-vs-pubsub)
- [**Kafka Components**](#kafka-components)
    - [Kafka Broker](#kafka-broker)
    - [Producer E Consumer](#producer-e-consumer)
    - [Kafka Partitions](#kafka-partitions)
    - [Consumer Group](#consumer-group)
- [**Exemplo 01**](#exemplo-01)
    - [Criar Tópico](#criar-tópico)
    - [Criar Producer](#criar-producer)
    - [Criar Consumer](#criar-consumer)
    - [Execuções](#execuções)


# O que é

O kafka também é um sistema de filas, assim como o RabbitMQ, então veja os conteúdos de [pub/sub](https://gitlab.com/e3666/backend-engineer/publish-subscribe) e [RabbitMQ](https://gitlab.com/e3666/backend-engineer/rabbitmq) para obter mais conhecimentos.

# Queue vs Pub/Sub

Antes de seguirmos, vamos só diferenciar essas duas coisas.

- Queue: A mensagem é publicada uma vez, e consumida uma vez, ou seja, quando ela é usada, ela é retirada da fila.
- Pub/Sub: A mensagem é uma vez, mas é consumida por muitos consumidores, ou seja, um consumidor pega uma informação, processa, o proximo, também pode querer usar essa mesma informação, logo ela deve ficar na fila.

E o kafka pode fazer os dois.


# Kafka Components

## Kafka Broker

É o servidor do kafka em que ficam as filas, e ele roda na porta 9092 por padrão. E dentro dele temos os Tópicos, que são partições em que escrevemos conteúdo

<img src='./images/fig_01.svg'/>

## Producer E Consumer

Veja que diferente do que o RabbitMQ faz, no kafka, o consumidor que tem que pedir algo para o broker, já no RabbitMQ o próprio server envia para o consumidor assim que algo chegar.

Um detalhe importante, é que o consumidor marca o que já leu, então ele não lê novamente, mas nada impede de outro consumidor ler a mesma mensagem (pub/sub).

<img src='./images/fig_02.svg'/>

> OBS.: Sim, as coisas entram de forma posicional, e tudo fica com um index, assim fica bem fácil e rápido de consumir, afinal está tudo sequêncial

## Kafka Partitions

Para facilitar a busca de informações dentro do tópico, o kafka particiona o tópico, assim fica mais simples de buscarmos informações para processar

<img src='./images/fig_03.svg'/>

O problema agora, é que o producer precisa saber em qual partição publicar algo.

## Consumer Group

Isso aqui foi criado para fazer com que o kafka fosse capaz de ser uma fila e um sistema de pub/sub

<img src='./images/fig_04.svg'/>

Como pode ver pela imagem, com o grupo criado, cada partição deve ter seu consumer, sendo assim não pode-se criar um terceiro consumer no exemplo.

E quando criamos um grupo, estamos fazendo com que o kafka se comporte como uma queue, pois quando um consumer lê uma posição de uma partição, ele não retorna mais nela, e como cada consumer lê de uma partição diferente, aquela mensagem jamais será lida novamente.


# Exemplo 01

[Arquivos...](./exemplo-01)

Vamos começar criando o container para o kafka, e também somos obrigados a criar um com o zookeeper, mas vai tudo ficar no mesmo arquivo `docker-compose.yml`.

- `docker-compose.yml`
```yml
version: "3"

services: 
    zookeeper:
        image: zookeeper
        ports: 
            - "2181:2181"

    kafka:
        image: confluentinc/cp-kafka 
        container_name: my-kafka
        ports: 
            - "9092:9092"
        depends_on: 
            - zookeeper
        environment: 
            KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
            KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://localhost:9092 
            KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
```

## Criar Tópico

Agora vamos criar um tópico com duas partições, a ideia é que uma partição pegue mensagens de A-M e a outra N-Z.
Para criar um tópico eu precisaria usar a API de admin da lib do python, mas por algum motivo ela não está funcionando muito bem, então vou criar o tópico pela linha de comando dentro do próprio container, para isso, você precisará ver o nome do container do kafka ai na sua máquina, por isso eu defini o nome do container no `docker-compose.yml`, para ficar igual aqui para mim e para você

```sh
# Criar um tópico
$ docker exec my-kafka kafka-topics --create --topic users --partitions 2 --bootstrap-server kafka:9092
# Confirmar que foi criado
$ docker exec my-kafka kafka-topics --describe --topic users --bootstrap-server kafka:9092
```

## Criar Producer

O producer não ficará no docker, então você pode criar uma venv e instalar a lib para trabalhar com o kafka no seu pc.

```sh
$ pip install kafka-python
```

```py
from sys import argv
from kafka import KafkaProducer

topic_name = 'users'
msg = argv[1]

# A-M = 0, N-Z = 1
partition = 0 if msg[0].upper() < 'N' else 1

producer = KafkaProducer(
    bootstrap_servers='localhost:9092'
)

# o valor precisa ser no formato binário
producer.send(
    topic=topic_name, 
    value=msg.encode(), 
    partition=partition
)

producer.flush()
```

## Criar Consumer

```py
from kafka import KafkaConsumer

topic_name = 'users'

consumer = KafkaConsumer(topic_name, group_id='my_group')
for msg in consumer:
    print(msg)
```

## Execuções

Vou deixar o container do kafka rodando, e vou instânciar um consumer, que vai receber todas as mensagens, e vou executando o producer passando nomes para ele, lembrando que nomes de A-M são partição 0, e de N-Z são da 1

> OBS.: O producer e o consumer estão rodando fora do docker, pois assim eu posso executar eles mais facilmente.

<img src='./images/fig_05.gif'/>

Agora vou criar um novo consumer, e veja que a partir do momento que o kafka entende que há um novo consumer no mesmo grupo, ele divide as partições

<img src='./images/fig_06.gif'/>

> OBS.: No consumer, existe o parâmetro `group_id='my_group'`, se você não passar esse parâmetro, o kafka não entende que os consumers são do mesmo grupo, e não começa a balancear as coisas, então o que vai acontecer, é que ambos os consumers receberão todas as mensagens.

# Referências

- [Apache Kafka Crash Course](https://www.youtube.com/watch?v=R873BlNVUB4&list=PLQnljOFTspQWGuRmwojJ6LiV0ejm6eOcs&index=21)
- [Apache Kafka — Aprendendo na prática](https://medium.com/trainingcenter/apache-kafka-codifica%C3%A7%C3%A3o-na-pratica-9c6a4142a08f)
- [kafka-python documentação](https://kafka-python.readthedocs.io/en/master/)
- [kafka documentação](https://kafka.apache.org/documentation/#quickstart)